if (online) {
if (location.protocol != 'https:'){
	window.location.replace('https://'+onlineDomaine+'/'+onlinePath+'/') // auto https pour ouvrir la webcam
}	 	
THREEx.ArToolkitContext.baseURL = '/'+onlinePath+'/'
}else{
	THREEx.ArToolkitContext.baseURL = '/'
}
var appLoaded=false;
if (loadingScreen==appLoaded) {appLoaded=true;};
initGifs();

/*
██╗███╗   ██╗██╗████████╗
██║████╗  ██║██║╚══██╔══╝
██║██╔██╗ ██║██║   ██║   
██║██║╚██╗██║██║   ██║   
██║██║ ╚████║██║   ██║   
╚═╝╚═╝  ╚═══╝╚═╝   ╚═╝   
*/
//////////////////////////////////////////////////////////////////////////////////
//		Init
//////////////////////////////////////////////////////////////////////////////////

// init renderer
var renderer	= new THREE.WebGLRenderer({
	antialias	: false,
	alpha: true
});
renderer.setClearColor(new THREE.Color('lightgrey'), 0)
// renderer.setPixelRatio( 1/2 );
renderer.setSize( window.innerWidth, window.innerHeight );
renderer.domElement.style.position = 'absolute'
renderer.domElement.style.top = '0px'
renderer.domElement.style.left = '0px'
renderer.domElement.setAttribute("id", "arLayer");
document.body.appendChild( renderer.domElement );

// array of functions for the rendering loop
var onRenderFcts= [];

// init scene and camera
var scene	= new THREE.Scene();

//////////////////////////////////////////////////////////////////////////////////
// lights
//////////////////////////////////////////////////////////////////////////////////
scene.add(new THREE.AmbientLight(0xbbbbbb));

var dirLight, dirLightHeper, hemiLight, hemiLightHelper
dirLight = new THREE.DirectionalLight( 0xffffff, 1 );
dirLight.color.setHSL( 0.1, 1, 0.95 );
dirLight.position.set( -1, 1.75, 1 );
dirLight.position.multiplyScalar( 30 );
scene.add( dirLight );
dirLight.castShadow = true;
dirLight.shadow.mapSize.width = 2048;
dirLight.shadow.mapSize.height = 2048;
var d = 50;
dirLight.shadow.camera.left = -d;
dirLight.shadow.camera.right = d;
dirLight.shadow.camera.top = d;
dirLight.shadow.camera.bottom = -d;
dirLight.shadow.camera.far = 3500;
dirLight.shadow.bias = -0.0001;
dirLightHeper = new THREE.DirectionalLightHelper( dirLight, 10 );
scene.add( dirLightHeper );

//////////////////////////////////////////////////////////////////////////////////
//		Initialize a basic camera
//////////////////////////////////////////////////////////////////////////////////

// Create a camera
var camera = new THREE.Camera();
scene.add(camera);

////////////////////////////////////////////////////////////////////////////////
//          handle arToolkitSource
////////////////////////////////////////////////////////////////////////////////

var arToolkitSource = new THREEx.ArToolkitSource({
	// to read from the webcam 
	sourceType : 'webcam',

	// to read from an image
	// sourceType : 'image',
	// sourceUrl : THREEx.ArToolkitContext.baseURL + '../data/images/img.jpg',		

	// to read from a video
	// sourceType : 'video',
	// sourceUrl : THREEx.ArToolkitContext.baseURL + '../data/videos/headtracking.mp4',		
})

arToolkitSource.init(function onReady(){	
	if (mirror) {
		document.getElementById('arVideo').classList.add("flip");
		document.getElementById('arLayer').classList.add("flip");
	};
	if (appLoaded==true) { 
		document.getElementById('arVideo').style.display = 'inline';
		document.getElementById('arLayer').style.display = 'inline';
		document.getElementById('wait').style.display = 'none';
	};
	onResize()		
})

// handle resize
window.addEventListener('resize', function(){
	onResize()
})
function onResize(){
	arToolkitSource.onResize()	
	arToolkitSource.copySizeTo(renderer.domElement)	
	if( arToolkitContext.arController !== null ){
		arToolkitSource.copySizeTo(arToolkitContext.arController.canvas)	
	}
	
}
////////////////////////////////////////////////////////////////////////////////
//          initialize arToolkitContext
////////////////////////////////////////////////////////////////////////////////

// create atToolkitContext
var arToolkitContext = new THREEx.ArToolkitContext({
	cameraParametersUrl: THREEx.ArToolkitContext.baseURL + 'data/camera_para.dat',
	detectionMode: 'mono',
	maxDetectionRate: 30,
	canvasWidth: 80*3,
	canvasHeight: 60*3,
})
// initialize it
arToolkitContext.init(function onCompleted(){
	// copy projection matrix to camera
	camera.projectionMatrix.copy( arToolkitContext.getProjectionMatrix() );
})

// update artoolkit on every frame
onRenderFcts.push(function(){
	if( arToolkitSource.ready === false )	return

	arToolkitContext.update( arToolkitSource.domElement )
})

/*
 █████╗  ██████╗████████╗██╗ ██████╗ ███╗   ██╗███████╗
██╔══██╗██╔════╝╚══██╔══╝██║██╔═══██╗████╗  ██║██╔════╝
███████║██║        ██║   ██║██║   ██║██╔██╗ ██║███████╗
██╔══██║██║        ██║   ██║██║   ██║██║╚██╗██║╚════██║
██║  ██║╚██████╗   ██║   ██║╚██████╔╝██║ ╚████║███████║
╚═╝  ╚═╝ ╚═════╝   ╚═╝   ╚═╝ ╚═════╝ ╚═╝  ╚═══╝╚══════╝                                             
*/
function markerOn(me){
	if(info) console.log(me+': on')	
	if (appLoaded==false) {return};

	eval(presets[me].jsIn);

	markers[me].alive = true
	markers[me].mp3.stop()

	if (markers[me].mp3._src=='sound/boop.mp3') {
		markers[me].mp3.volume(0.7)
		markers[me].mp3._rate = 0.7+Math.random()*0.6
	};

	markers[me].mp3.play()
	if ( (markers[me].type=="gif")&&(superGifs[presets[me].img].get_loading()==false) ) {
		// bug
		superGifs[presets[me].img].pause();
		superGifs[presets[me].img].play();
	};
}

function markerOff(me){
	if(info) console.log(me+': off')	
	if (appLoaded==false) {return};

	eval(presets[me].jsOut);

	markers[me].alive = false
    markers[me].mp3.stop()

	if (markers[me].mp3._src=='sound/boop.mp3') {
		markers[me].mp3.loop(false)
	    markers[me].mp3.volume(0.3)
		markers[me].mp3._rate = 0.4+Math.random()*0.3
		markers[me].mp3.play()
		
	};


	if (markers[me].type=="gif") {
		// bug
		superGifs[presets[me].img].pause();
	};
}

/*
████████╗██╗   ██╗██████╗ ███████╗███████╗
╚══██╔══╝╚██╗ ██╔╝██╔══██╗██╔════╝██╔════╝
   ██║    ╚████╔╝ ██████╔╝█████╗  ███████╗
   ██║     ╚██╔╝  ██╔═══╝ ██╔══╝  ╚════██║
   ██║      ██║   ██║     ███████╗███████║
   ╚═╝      ╚═╝   ╚═╝     ╚══════╝╚══════╝	 
   types                                         
*/

// cube
function cube(options, parentObj){
	var that=this
	this.geometry	= new THREE.CubeGeometry(1,1,1);
	this.material	= new THREE.MeshNormalMaterial({
		transparent : options.transparent,
		opacity: options.opacity,
		side: options.side
	}); 
	this.mesh	= new THREE.Mesh( this.geometry, this.material );
	this.mesh.position.y	= this.geometry.parameters.height/2;
	parentObj.arWorldRoot.add( that.mesh );
	onRenderFcts.push(function(){if (that.mesh.parent.visible) {eval(options.jsHover);};})		
}

// torus
function torus(options, parentObj){
	var that=this
	this.geometry	= new THREE.TorusKnotGeometry(0.3,0.1,64,16);
	this.material	= new THREE.MeshNormalMaterial({
		transparent : options.transparent,
		opacity: options.opacity,
		side: options.side
	}); 
	this.mesh	= new THREE.Mesh( this.geometry, this.material );
	this.mesh.position.y	= options.scale / 2		
	this.mesh.scale.x = options.scale
	this.mesh.scale.y = options.scale
	this.mesh.scale.z = options.scale
	onRenderFcts.push(function(){if (that.mesh.parent.visible) {eval(options.jsHover);};})
	parentObj.arWorldRoot.add( that.mesh );
}

// iso
function iso(options, parentObj){
	var that=this
	this.geometry = new THREE.IcosahedronGeometry();
	
	this.material	= new THREE.MeshNormalMaterial({
		transparent : options.transparent,
		opacity: options.opacity,
		side: options.side
	}); 
	this.mesh	= new THREE.Mesh( this.geometry, this.material );	
	onRenderFcts.push(function(){if (that.mesh.parent.visible) {eval(options.jsHover);};})
	parentObj.arWorldRoot.add( that.mesh );
}


// ring
function ring(options, parentObj){
	var that=this
	this.geometry = new THREE.TorusGeometry( 10, 3, 16, 100 );	
	this.material	= new THREE.MeshNormalMaterial({
		transparent : options.transparent,
		opacity: options.opacity,
		side: options.side
	}); 
	this.mesh	= new THREE.Mesh( this.geometry, this.material );
	this.mesh.scale.x = options.scale
	this.mesh.scale.y = options.scale
	this.mesh.scale.z = options.scale
	onRenderFcts.push(function(){if (that.mesh.parent.visible) {eval(options.jsHover);};})
	parentObj.arWorldRoot.add( that.mesh );
}

// earth
function earth(options, parentObj){
	var that=this
	this.mesh = new THREE.Object3D()
	this.loader = new THREE.TextureLoader();
	this.loader.load( '3d/terre.jpg', function ( texture ) {
		that.geometry = new THREE.SphereGeometry( 0.8, 32, 32 );
		that.material = new THREE.MeshBasicMaterial( { map: texture, overdraw: 0.5 } );
		that.mesh = new THREE.Mesh( that.geometry, that.material );
		that.mesh.position.y = 1
		that.mesh.rotation.x = -45
		that.mesh.rotation.y = 0
		that.mesh.rotation.z = 0
		onRenderFcts.push(function(){if (that.mesh.parent.visible) {eval(options.jsHover);};})
		parentObj.arWorldRoot.add( that.mesh )	
	} );
	this.loader.magFilter = THREE.NearestFilter;
	this.loader.minFilter = THREE.LinearMipMapLinearFilter;
	this.loader.anisotropy = renderer.getMaxAnisotropy();
}

// pix
function pix(options, parentObj){
	var that=this
	this.mesh = new THREE.Object3D();
	this.loader = new THREE.TextureLoader();
	this.loader.magFilter = THREE.NearestFilter;
	this.loader.minFilter = THREE.LinearFilter;
	this.loader.anisotropy = renderer.getMaxAnisotropy();	

	this.loader.load( options.img, function ( texture ) {
		that.geometry	= new THREE.PlaneBufferGeometry( 1, 1 );
		that.material	= new THREE.MeshBasicMaterial({
			map: texture,
			opacity: options.opacity,
			side: THREE.DoubleSide,
			transparent: true,
	        needsUpdate: true,
	        depthWrite: false, 
	        depthTest: false
		}); 
		that.mesh = new THREE.Mesh( that.geometry, that.material );
		that.mesh.position.y = 0;
		that.mesh.rotation.x = -90 / 360 * (Math.PI * 2);
		onRenderFcts.push(function(){if (that.mesh.parent.visible) {eval(options.jsHover);};})
		parentObj.arWorldRoot.add( that.mesh );	
		
	    w= 1;
	    h= (w/that.material.map.image.width)*that.material.map.image.height; 
	    that.mesh.scale.x = w * options.scale;
	    that.mesh.scale.y = h * options.scale;
	    
	} );
}

// nopix
function nopix(options, parentObj){
	var that=this
	this.mesh = new THREE.Object3D();
}

// trail
function trail(options, parentObj){
	var that=this
	this.mesh = new THREE.Object3D();
	this.trails = new THREE.Group();
	this.loader = new THREE.TextureLoader();
	this.loader.magFilter = THREE.NearestFilter;
	this.loader.minFilter = THREE.LinearFilter;
	this.loader.anisotropy = renderer.getMaxAnisotropy();	
	this.loader.load( options.img, function ( texture ) {
		that.geometry	= new THREE.PlaneBufferGeometry( 1, 1 );
		that.material	= new THREE.MeshBasicMaterial({
			map: texture,
			opacity: options.opacity,
			side: THREE.DoubleSide,
			transparent: true,
	        needsUpdate: true,
	        depthWrite: false, 
	        depthTest: false
		}); 
		/*
		that.mesh = new THREE.Mesh( that.geometry, that.material );
		that.mesh.position.y = 0;
		that.mesh.rotation.x = -90 / 360 * (Math.PI * 2);
		w= 1;
   		h= (w/texture.image.width)*texture.image.height; 
   		that.mesh.scale.x = w;
   		that.mesh.scale.y = h;
   		*/
		var w= options.scale;
   		var h= (w/texture.image.width)*texture.image.height; 
   		var planeSize=80;
		onRenderFcts.push(function(){
			if (that.mesh.parent.visible) {
				if ((that.trails.children.length>options.iterations)&&(options.iterations!=0)) {
					that.trails.remove(that.trails.children[0]);
				};
				//spawn
				var tgeometry = new THREE.PlaneGeometry( planeSize, planeSize );
				var tmaterial = new THREE.MeshBasicMaterial( {
					map: texture,
					opacity: options.opacity,
					side: THREE.DoubleSide,
					transparent: true,
			        needsUpdate: true,
			        depthWrite: false, 
			        depthTest: false
				} );
				var tplane = new THREE.Mesh( tgeometry, tmaterial );
				tplane.position.x = parentObj.markerRoot.position.x * planeSize;
				tplane.position.y = parentObj.markerRoot.position.y * planeSize;
				tplane.position.z = parentObj.markerRoot.position.z * planeSize;

				//tplane.rotation.x = parentObj.markerRoot.rotation.x-(90 / 360 * (Math.PI * 2));
				mirrorAdd=0;
				if (mirror==false) {mirrorAdd=(180 / 360 * (Math.PI * 2))};
				tplane.rotation.x = parentObj.markerRoot.rotation.x-(90 / 360 * (Math.PI * 2));
				tplane.rotation.y = (180 / 360 * (Math.PI * 2))-parentObj.markerRoot.rotation.z+mirrorAdd;
				tplane.rotation.z = parentObj.markerRoot.rotation.y;
	       		tplane.scale.x = w;
	       		tplane.scale.y = h;
				that.trails.add( tplane );
				eval(options.jsHover);
			}else{
				if (that.trails.children.length>0) { for (var i = that.trails.children.length - 1; i >= 0; i--) {that.trails.remove(that.trails.children[i])}};
			}
		})
		parentObj.arWorldRoot.add( that.mesh );	
		scene.add( that.trails );	
	} );
}

// gif
function gif(options, parentObj){
	var that=this
	this.loaded = false;
	this.mesh = new THREE.Object3D();
	this.geometry	= new THREE.PlaneBufferGeometry( 1, 1 );
	this.material	= new THREE.MeshBasicMaterial({
			map: gifsTexture[options.img],
			opacity: options.opacity,
			side: THREE.DoubleSide,
			transparent: true,
	        needsUpdate: true,
	        depthWrite: false, 
	        depthTest: false
		}); 
	this.mesh = new THREE.Mesh( this.geometry, this.material );
	this.mesh.position.y = 0;
	this.mesh.rotation.x = -90 / 360 * (Math.PI * 2);
	onRenderFcts.push(function(){
		if (that.mesh.parent.visible) {
			if (superGifs[options.img].get_loading()==false) {
				gifsTexture[options.img].needsUpdate=true;						
			};
			eval(options.jsHover);
		}		

	})
	parentObj.arWorldRoot.add( that.mesh );	
}

// TEXT
var group, textMesh1, textMesh2, textGeo, materials;
	var height = 0.5,
	size = 2,
	hover = 10,
	curveSegments = 0.1,
	bevelThickness = 0.1,
	bevelSize = 0.1,
	bevelSegments = 0.1,
	bevelEnabled = true,
	font = undefined,
	fontName = "helvetiker", // helvetiker, optimer, gentilis, droid sans, droid serif
	fontWeight = "bold"; // normal bold
var fontMap = {
	"helvetiker": 0,
	"optimer": 1,
	"gentilis": 2,
	"droid/droid_sans": 3,
	"droid/droid_serif": 4
};
var weightMap = {
	"regular": 0,
	"bold": 1
};
var reverseFontMap = [];
var reverseWeightMap = [];
for ( var i in fontMap ) reverseFontMap[ fontMap[i] ] = i;
for ( var i in weightMap ) reverseWeightMap[ weightMap[i] ] = i;
function loadFont(parent, txt) {
	var loader = new THREE.FontLoader();
	loader.load( 'fonts/' + fontName + '_' + fontWeight + '.typeface.json', function ( response ) {
		font = response;
		refreshText(parent,txt);
	} );
}
function createText(parent,txt) {
	textGeo = new THREE.TextGeometry( txt, {
		font: font,
		size: size,
		height: height,
		curveSegments: curveSegments,
		bevelThickness: bevelThickness,
		bevelSize: bevelSize,
		bevelEnabled: bevelEnabled

	});
	textGeo.computeBoundingBox();
	textGeo.computeVertexNormals();
	var centerOffset = -0.5 * ( textGeo.boundingBox.max.x - textGeo.boundingBox.min.x );
	textMesh1 = new THREE.Mesh( textGeo, materials );
	textMesh1.rotation.x = - 90 / 360 * (Math.PI * 2)
	if (mirror) {
		textMesh1.rotation.y = - 180 / 360 * (Math.PI * 2)
		textMesh1.rotation.z = - 180 / 360 * (Math.PI * 2)			
	};
	/*
	textMesh1.position.x = - size - 0.7
	textMesh1.position.y = -.1
	textMesh1.position.z = 0.25
	*/
	textMesh1.scale.x = textMesh1.scale.y = textMesh1.scale.z = 0.5
	parent.add( textMesh1 );
}

function refreshText(parent,txt) {
	parent.remove( textMesh1 );
	createText(parent,txt);
}
// text 
function texte(options, parentObj){
	var that=this
	this.text = options.text
	color = 0xFF1493;
	materials = [
		new THREE.MeshPhongMaterial( { color: color } ), // front
		new THREE.MeshPhongMaterial( { color: color } ) // side
	];
	this.mesh = new THREE.Group()
	this.mesh.position.x = 0;
	this.mesh.position.y = 0;
	this.mesh.position.z = 0;
	parentObj.arWorldRoot.add( this.mesh );
	loadFont(this.mesh,this.text);
}

// model
function model3d(options, parentObj) {
	var that=this
	this.mesh = new THREE.Object3D()
	this.modelLoaded = false
	this.modelLoader = new THREE.ObjectLoader()
	this.modelLoader.load(options.modelUrl, function ( obj ) { 
		that.mesh = obj
		that.modelLoaded = true
		that.mesh.scale.x = options.scale
		that.mesh.scale.y = options.scale
		that.mesh.scale.z = options.scale
		that.mesh.position.x = options.posX
		that.mesh.position.y = options.posY
		that.mesh.position.z = options.posZ
		that.mesh.rotation.x = options.rotX / 360 * (Math.PI * 2)
		that.mesh.rotation.y = options.rotY / 360 * (Math.PI * 2)
		that.mesh.rotation.z = options.rotZ / 360 * (Math.PI * 2)
		onRenderFcts.push(function(){if (that.mesh.parent.visible) {eval(options.jsHover);};})
		parentObj.arWorldRoot.add( that.mesh )	
	});
}

/*
███╗   ███╗ █████╗ ██████╗ ██╗  ██╗███████╗██████╗ ███████╗
████╗ ████║██╔══██╗██╔══██╗██║ ██╔╝██╔════╝██╔══██╗██╔════╝
██╔████╔██║███████║██████╔╝█████╔╝ █████╗  ██████╔╝███████╗
██║╚██╔╝██║██╔══██║██╔══██╗██╔═██╗ ██╔══╝  ██╔══██╗╚════██║
██║ ╚═╝ ██║██║  ██║██║  ██║██║  ██╗███████╗██║  ██║███████║
╚═╝     ╚═╝╚═╝  ╚═╝╚═╝  ╚═╝╚═╝  ╚═╝╚══════╝╚═╝  ╚═╝╚══════╝                                                           
*/

var markers=[];
//var patternUrls=['data/02.pat','data/06.pat']
function marker(options) {
	var that = this
	this.id = options.id
	this.type = options.type
	this.alive = false
	this.scale = options.scale
	this.posX = options.posX
	this.posY = options.posY
	this.posZ = options.posZ
	this.rotX = options.rotX

	this.rotY = options.rotY
	if (mirror) {this.rotY=(this.rotY+180)};	


	this.rotZ = options.rotZ
	this.rotSpeedX = options.rotSpeedX
	this.rotSpeedY = options.rotSpeedY
	this.rotSpeedZ = options.rotSpeedZ
	// Create a ArMarkerControls
	this.markerRoot = new THREE.Group

	scene.add(this.markerRoot)
	this.artoolkitMarker = new THREEx.ArMarkerControls(arToolkitContext, this.markerRoot, {
		type : 'pattern',
		patternUrl : THREEx.ArToolkitContext.baseURL + options.patternUrl
	})	    
	// build a smoothedControls
	this.smoothedRoot = new THREE.Group()
	scene.add(this.smoothedRoot)
	this.smoothedControls = new THREEx.ArSmoothedControls(this.smoothedRoot, {
		lerpPosition: 0.4,
		lerpQuaternion: 0.3,
		//lerpScale: 1,
	})
	onRenderFcts.push(function(delta){
		that.smoothedControls.update(that.markerRoot,that.id)
	})

	/*
	 █████╗ ██████╗ ██████╗      ██████╗ ██████╗      ██╗███████╗ ██████╗████████╗
	██╔══██╗██╔══██╗██╔══██╗    ██╔═══██╗██╔══██╗     ██║██╔════╝██╔════╝╚══██╔══╝
	███████║██║  ██║██║  ██║    ██║   ██║██████╔╝     ██║█████╗  ██║        ██║   
	██╔══██║██║  ██║██║  ██║    ██║   ██║██╔══██╗██   ██║██╔══╝  ██║        ██║   
	██║  ██║██████╔╝██████╔╝    ╚██████╔╝██████╔╝╚█████╔╝███████╗╚██████╗   ██║   
	╚═╝  ╚═╝╚═════╝ ╚═════╝      ╚═════╝ ╚═════╝  ╚════╝ ╚══════╝ ╚═════╝   ╚═╝   
    */
	// add an object in the scene
	this.arWorldRoot = this.smoothedRoot

	if (this.type=="cube") {
		this.obj = new cube(options, that)
		//this.arWorldRoot.add( this.obj.mesh )	
	};

	if (this.type=="earth") {
		this.obj = new earth(options, that)
	};

	if (this.type=="pix") {
		this.obj = new pix(options, that)
	};			

	if (this.type=="nopix") {
		this.obj = new nopix(options, that)
	};		

	if (this.type=="trail") {
		this.obj = new trail(options, that)
	};

	if (this.type=="gif") {
		this.obj = new gif(options, that)
	};

	if (this.type=="iso") {
		this.obj = new iso(options, that)
		//this.arWorldRoot.add( this.obj.mesh )	
	};

	if (this.type=="ring") {
		this.obj = new ring(options, that)
		//this.arWorldRoot.add( this.obj.mesh )	
	};

	if (this.type=="texte") {
		this.obj = new texte(options, that)
	};

	if (this.type=="torus") {
		this.obj = new torus(options, that)
		//this.arWorldRoot.add( this.obj.mesh )	
	};

	if (this.type=="model3d") {
		this.obj = new model3d(options, that)	
	};

	/* morphTarget */

	//markers[0].obj.mesh.geometry.vertices.length
	//this.obj.mesh.geometry.vertices.length

	/*
	██████╗  ██████╗ ███████╗    ██╗██████╗  ██████╗ ████████╗ ██╗███████╗ ██████╗ █████╗ ██╗     ███████╗
	██╔══██╗██╔═══██╗██╔════╝   ██╔╝██╔══██╗██╔═══██╗╚══██╔══╝██╔╝██╔════╝██╔════╝██╔══██╗██║     ██╔════╝
	██████╔╝██║   ██║███████╗  ██╔╝ ██████╔╝██║   ██║   ██║  ██╔╝ ███████╗██║     ███████║██║     █████╗  
	██╔═══╝ ██║   ██║╚════██║ ██╔╝  ██╔══██╗██║   ██║   ██║ ██╔╝  ╚════██║██║     ██╔══██║██║     ██╔══╝  
	██║     ╚██████╔╝███████║██╔╝   ██║  ██║╚██████╔╝   ██║██╔╝   ███████║╚██████╗██║  ██║███████╗███████╗
	╚═╝      ╚═════╝ ╚══════╝╚═╝    ╚═╝  ╚═╝ ╚═════╝    ╚═╝╚═╝    ╚══════╝ ╚═════╝╚═╝  ╚═╝╚══════╝╚══════╝
	                                                                                                      
	*/
	
	/*
	this.obj.mesh.scale.x = this.scale
	this.obj.mesh.scale.y = this.scale
	this.obj.mesh.scale.z = this.scale
	*/

	// position + rotation
	if (this.posX!=0) this.obj.mesh.position.x = this.posX
	if (this.posY!=0) this.obj.mesh.position.y = this.posY
	if (this.posZ!=0) this.obj.mesh.position.z = this.posZ
	if (this.rotX!=0) this.obj.mesh.rotation.x = this.rotX / 360 * (Math.PI * 2)
	if (this.rotY!=0) this.obj.mesh.rotation.y = this.rotY / 360 * (Math.PI * 2)
	if (this.rotZ!=0) this.obj.mesh.rotation.z = this.rotZ / 360 * (Math.PI * 2)
	// rotation speed
	onRenderFcts.push(function(){
		if (that.rotSpeedX!=0) that.obj.mesh.rotation.x += that.rotSpeedX
		if (that.rotSpeedY!=0) that.obj.mesh.rotation.y += that.rotSpeedY
		if (that.rotSpeedZ!=0) that.obj.mesh.rotation.z += that.rotSpeedZ			
	})	

	this.mp3 = new Howl({
	    urls: [options.mp3],
	    autoplay: options.autoplay,
	    loop: options.loop,
	    volume: options.volume,
	    onload: function() { },      
	    onend: function() { }
	});
}
//
function markerUpdate() {
	/*
    for(var i=0; i<markers.length; i++){
		// do something...
    	// markers[i].obj.mesh.	    	
    }
    */
}

for (var i = 0; i < presets.length; i++) {
	presets[i].patternUrl = markerPath+'/'+i+'.pat'
};

for (var i = 0; i < presets.length; i++) {
	// default
	options = {
		id: i,
		patternUrl: '',
		type: 'cube',
		modelUrl: '',
		img: 0,
		scale: 1,
		posX : 0,
		posY : 0,
		posZ : 0,
		rotX : 0,
		rotY : 0,
		rotZ : 0,
		rotSpeedX : 0,
		rotSpeedY : 0,
		rotSpeedZ : 0,
		transparent : false,
		text : "hello",
		opacity: 1,
		side: THREE.DoubleSide,		
	    mp3: 'sound/boop.mp3',
	    autoplay: false,
	    loop: false,
	    volume: 0.7,
	    iterations: 0,
	    jsIn: "",
	    jsHover: "",
	    jsOut: "",
	}
	for(var o=0;o<Object.keys(options).length;o++){if(presets[i][Object.keys(options)[o]]!=undefined)options[Object.keys(options)[o]]=presets[i][Object.keys(options)[o]]}

	temp = new marker(options);
	markers.push(temp); 

};

//utils
function toggleFullScreen() {
  if (fullScreen) {
	  var doc = window.document;
	  var docEl = doc.documentElement;

	  var requestFullScreen = docEl.requestFullscreen || docEl.mozRequestFullScreen || docEl.webkitRequestFullScreen || docEl.msRequestFullscreen;
	  var cancelFullScreen = doc.exitFullscreen || doc.mozCancelFullScreen || doc.webkitExitFullscreen || doc.msExitFullscreen;

	  if(!doc.fullscreenElement && !doc.mozFullScreenElement && !doc.webkitFullscreenElement && !doc.msFullscreenElement) {
	    requestFullScreen.call(docEl);
	  }
	  else {
	    //cancelFullScreen.call(doc);
	  }
  };
}

//////////////////////////////////////////////////////////////////////////////////
//		render the whole thing on the page
//////////////////////////////////////////////////////////////////////////////////
//var stats = new Stats();
//document.body.appendChild( stats.dom );
// render the scene
onRenderFcts.push(function(){
	markerUpdate();
	renderer.render( scene, camera );
	//stats.update();
})

// run the rendering loop
var lastTimeMsec= null
requestAnimationFrame(function animate(nowMsec){
	// keep looping
	requestAnimationFrame( animate );
	// measure time
	lastTimeMsec	= lastTimeMsec || nowMsec-1000/60
	var deltaMsec	= Math.min(200, nowMsec - lastTimeMsec)
	lastTimeMsec	= nowMsec
	// call each update function
	onRenderFcts.forEach(function(onRenderFct){
		onRenderFct(deltaMsec/1000, nowMsec/1000)
	})
})

	$(function() {
	// jquery ready
	});

window.onload = function() {
    // window loaded
}