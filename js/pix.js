//console.log("Let's cook!");

var xx = 0;
for (var i = 0; i < presets.length; i++) {
	if (presets[i].type=="gif") {
		$('#gifs').append( '<img id="gif'+xx+'" src="'+presets[i].img+'" rel:auto_play="0">' );
		presets[i].img = xx;
		xx = xx + 1;
	};
};

// gifs 
var superGifs = [], gifsSource = [], gifsUrl = [], gifsTexture = [], gifsCanvas = [], gifsContext = [];
var gifsLoaded = 0;


function gifSize(i){
    w= 1;
    h= (w/markers[i].obj.mesh.material.map.image.width)*markers[i].obj.mesh.material.map.image.height; 
    markers[i].obj.mesh.scale.x = w * markers[i].scale;
    markers[i].obj.mesh.scale.y = h * markers[i].scale;    
}

function initGifs(){
	i = 0;
	jQuery('#gifs img').each(function(index) {
		that=this;
       	gifsSource.push(jQuery(this).attr('src'));
       	gifName = 'gif'+i;
       	gifsUrl.push(window.location.origin+THREEx.ArToolkitContext.baseURL+gifsSource[i]);
       	superGifs[i] = new SuperGif({ gif: document.getElementById(gifName) } );
       	superGifs[i].load(function(gif){
       		    for (var j = 0; j < markers.length; j++) {
       				if (presets[j].img==index) {
                // fix for some weird texture bug...
                gifSize(j);
                gifsLoaded = gifsLoaded + 1;
       					if (gifsLoaded==superGifs.length) {                                         
       						//console.log('all gifs loaded :3');
                  $('#arVideo').show()
                  $('#arLayer').show()
                  $('#wait').hide()
                  appLoaded=true;
                  onResize();
       					};
       			};
       		};    		
       	});
       	canvas = document.getElementById(gifsUrl[i]);
       	gifsTexture[i] = new THREE.Texture(canvas);
       	gifsTexture[i].needsUpdate=true;
       	gifsTexture[i].anisotropy = 1;
		gifsTexture[i].magFilter = THREE.NearestFilter;
		gifsTexture[i].minFilter = THREE.LinearFilter;
       	gifsCanvas[i] = superGifs[i].get_canvas();
       	gifsContext[i] = gifsCanvas[i].getContext("2d");
       	i = i+1;
   	});
}