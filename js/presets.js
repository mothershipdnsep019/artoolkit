/*
██████╗ ██████╗ ███████╗███████╗███████╗████████╗███████╗
██╔══██╗██╔══██╗██╔════╝██╔════╝██╔════╝╚══██╔══╝██╔════╝
██████╔╝██████╔╝█████╗  ███████╗█████╗     ██║   ███████╗
██╔═══╝ ██╔══██╗██╔══╝  ╚════██║██╔══╝     ██║   ╚════██║
██║     ██║  ██║███████╗███████║███████╗   ██║   ███████║
╚═╝     ╚═╝  ╚═╝╚══════╝╚══════╝╚══════╝   ╚═╝   ╚══════╝
*/

var presets=[];
// tosti
//for (var i = 0; i < 8; i++) { presets[i]={} };
presets = [

	// 0
	{
		type: 'model3d',
		modelUrl: '3d/david2.json',
		scale: 0.06,
		posX: -0.3,
		posY: 3,
		posZ: 0.3,
		rotX: -90,
		rotY: 0,
		rotZ: 0
	}

	// 1
	,{
		type: 'torus',
		scale: 2,
		posY: 1,
		rotSpeedX: 0.01,
		rotSpeedY: -0.001,
		rotSpeedZ: 0.0001,
		js: ""
	}

	// 2
	,{
		type: 'ring',
		scale: 0.09,
		posY: 1,
		rotSpeedY: 0.1
	}
	
	// 3
	,{
		type: 'iso',
		scale: 1,
		posY: 0.5		
	}
	
	// 4
	,{
		type: 'cube'
	}
	
	// 5
	,{
		type: 'earth',
		rotSpeedY: -0.05,
		js: ""
	}
	
	// 6
	,{
		type: 'texte',
		scale: 1
	}
	
	// 7
	,{
		type: 'model3d',
		modelUrl: '3d/coloneB.json',
		scale: 0.01,
		posX: -0.48,
		posY: -0.1,
		posZ: 1.1,
		rotX: -90,
		rotY: 0,
		rotZ: 0
	}

	// 8
	,{
		type: 'pix',
		img: 'img/puppy.png',
		rotSpeedY: -0.1,
		js: ''
	}
	
	// 9
	,{
		type: 'pix',
		img: 'img/poop.png',
		//rotSpeedY: -0.1,
		js: ''
	}
	
	// 10
	,{
		type: 'gif',
		img: 'gif/CAT9.gif',
		mp3:'sound/scratch.mp3',
		loop: true,
		js: ''
	}

	// 11
	,{
		type: 'gif',
		img: 'gif/_0.gif',
		js: ''
	}

	// 12
	,{
		type: 'trail',
		img: 'img/error.png',
		//rotSpeedY: -0.1,
		js: ''
	}


];