// ABC DEMO
presets = [

	// 0 - A
	{
		type: 'gif',
		img: 'gif/azerty.gif',
		mp3: 'sound/azerty.mp3',
		loop: true,
		scale: 3,
		posZ: 0.1,
		posY: 0.1,
		jsIn: '',
	},

	// 1 - B
	{
		type: 'nopix',
		mp3: 'sound/bug.mp3',
		loop: false,
		scale: 1.1,
		jsIn: '$("#bsod").show()',
		jsOut: '$("#bsod").hide()',
	},

	// 2 - C
	{
		type: 'gif',
		img: 'gif/code.gif',
		mp3: 'sound/code.mp3',
		loop: true,
		scale: 1,
	},
	
	// 3 - D
	{
		type: 'gif',
		img: 'gif/disquette.gif',
		mp3: 'sound/disquette.mp3',
		loop: true,
		scale: 1,
	},

	// 4 - E
	{
		type: 'pix',
		img: 'img/emoji.png',
		mp3: 'sound/emoji.mp3',
		loop: false,
		scale: 1,
		rotSpeedZ: -0.2,
	},

	// 5 - F
	{
		type: 'trail',
		img: 'img/window.png',
		scale: 2,
		js: '',
	},

	// 6 - G
	{
		type: 'gif',
		img: 'gif/gameboy.gif',
		scale: 2,
		mp3: 'sound/gameboy.mp3',
		loop: true,
		js: '',
	},

	// 7 - H
	{
		type: 'gif',
		img: 'gif/hacker.gif',
		scale: 1,
		mp3: 'sound/hacker.mp3',
		loop: false,
		js: '',
	},

	// 8 - I
	{
		type: 'earth',
		rotSpeedY: -0.05,
		mp3: 'sound/internet.mp3',
		loop: true,
		js: "",
	},

	// 9 - J
	{
		type: 'gif',
		img: 'gif/jeu-video.gif',
		scale: 4,
		mp3: 'sound/jeu-video.mp3',
		loop: true,
		js: '',
		rotZ:90,
	},

	// 10 - K
	{
		type: 'gif',
		img: 'gif/kilo-octet.gif',
		scale: 2,
		mp3: 'sound/kilo-octet.mp3',
		loop: false,
		js: '',
	},

	// 11 - L
	{
		type: 'gif',
		img: 'gif/logiciel.gif',
		scale: 1,
		mp3: 'sound/logiciel.mp3',
		loop: true,
		js: '',
	},

	// 12 - M
	{
		type: 'gif',
		img: 'gif/minitel.gif',
		scale: 3,
		mp3: 'sound/minitel.mp3',
		loop: false,
		js: '',
	},

	// 13 - N
	{
		type: 'gif',
		img: 'gif/numerique.gif',
		scale: 3.5,
		mp3: 'sound/numerique.mp3',
		loop: true,
		js: '',
		rotZ: 90,
	},

	// 14 - O
	{
		type: 'pix',
		img: 'img/obsolescence.png',
		mp3: 'sound/obsolescence-programmee.mp3',
		loop: false,
		scale: 4,
		js: '',
	},

	// 15 - P
	{
		type: 'gif',
		img: 'gif/pixel.gif',
		scale: 3,
		mp3: 'sound/pixel.mp3',
		loop: false,
		js: '',
		posX: -.4,
		posZ: -0.2,
	},

	// 16 - Q
	{
		type: 'gif',
		img: 'gif/qrcode.gif',
		scale: 1,
		mp3: 'sound/qrcode.mp3',
		loop: false,
		js: '',
	},

	// 17 - R
	{
		type: 'gif',
		img: 'gif/reseau-social.gif',
		scale: 2,
		mp3: 'sound/reseau-social.mp3',
		loop: false,
		js: '',
		posZ: -0.2,
	},

	// 18 - S
	{
		type: 'gif',
		img: 'gif/spam.gif',
		scale: 2.6,
		mp3: 'sound/spam.mp3',
		loop: false,
		js: '',
	},
	
	// 19 - T
	{
		type: 'gif',
		img: 'gif/troll.gif',
		scale: 4,
		mp3: 'sound/troll.mp3',
		loop: false,
		js: '',
	},

	
	// 20 - U
	{
		type: 'gif',
		img: 'gif/usb.gif',
		scale: 1,
		mp3: 'sound/usb.mp3',
		loop: false,
		js: '',
	},

	// 21 - V
	{
		type: 'gif',
		img: 'gif/virus.gif',
		scale: 1.5,
		mp3: 'sound/virus.mp3',
		loop: false,
		js: '',
	},
	
	// 22 - W
	{
		type: 'gif',
		img: 'gif/webcam.gif',
		scale: 1.65,
		mp3: 'sound/webcam.mp3',
		loop: false,
		js: '',
	},
	
	// 23 - X
	{
		type: 'gif',
		img: 'gif/x.gif',
		scale: 2,
		mp3: 'sound/x.mp3',
		loop: false,
		js: '',
		posZ: -0.3,
	},

		
	// 24 - Y
	{
		type: 'gif',
		img: 'gif/youtube.gif',
		scale: 2.6,
		mp3: 'sound/youtube.mp3',
		loop: false,
		js: '',
	},
		
	// 25 - Z
	{
		type: 'gif',
		img: 'gif/zip.gif',
		scale: 2,
		mp3: 'sound/zip.mp3',
		loop: false,
		js: '',
		posX:  0.32,
		posZ: 0.05,
	},

]