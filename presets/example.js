// example de preset
monPreset = {
	type: 'cube',					// type d'objet : pix, gif, model3d, cube, ring, iso, earth, texte, trail.
	img: 'img/poop.png'	,			// chemin du fichier (img, gif, ou trail)
	scale: 1,						// échelle de l'objet par rapport au marker

	modelUrl: '',					// chemin du modèle 3d au format .json (model3d)

	posX : 0,						// ajustement position X
	posY : 0,						// ajustement position Y
	posZ : 0,						// ajustement position Z
	rotX : 0,						// ajustement rotation X
	rotY : 0,						// ajustement rotation Y
	rotZ : 0,						// ajustement rotation Z

	rotSpeedX : 0,					// vitesse de rotation automatique X
	rotSpeedY : 0,					// vitesse de rotation automatique Y
	rotSpeedZ : 0,					// vitesse de rotation automatique Z

	transparent : false,			// transparence (true/false)
	opacity: 1,						// opacité de l'objet

	text : "hello",					// message à afficher (texte)

    mp3: 'sound/boop.mp3',			// chemin du fichier son
    loop: false,					// jouer fichier son en boucle (true/false)
    volume: 0.7,					// volume du fichier (0-1)

    jsIn: "console.log('hi');",		// javascript exécuté quand l'objet apparaît
    jsHover: "console.log('bye');", // javascript exécuté quand l'objet disparaît
    jsOut: "",						// javascript exécuté en boucle quand l'objet est visible

    iterations: 0,					// nombre d'iteration de l'objet trail
}
