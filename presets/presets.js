presets[0] = {
	type: 'pix',
	img: 'img/emoji.png',
	mp3: 'demo/sncf-echo.mp3',
	loop : true

}

presets[1] = {
	type: 'pix',
	img: 'img/emoji.png',
	mp3: 'demo/drumloop.mp3',
	loop: true
}

presets[2] = {
	type: 'pix',
	img: 'img/emoji.png',
	jsIn:'bloop2.play()',
	jsOut: 'bloop2.pause()',
	mp3: '',
	volume: 0.4
}

presets[3] = {
	type: 'pix',
	img: 'img/emoji.png',
	jsIn:'bloop3.play()',
	jsOut: 'bloop3.pause()',
	mp3: '',
	volume: 0.4
}
